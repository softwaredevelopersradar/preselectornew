﻿using System;
using System.Linq;

namespace WorkPortNew
{
    /// <summary>
    /// Base class for connect to preselector.
    /// </summary>
    public abstract class PreselModel
    {
        #region Event`s

        public abstract event EventHandler<string> OnRead;
        
        public abstract event EventHandler<string> OnWrite;

        public event EventHandler<PreselectorNumber> OnPreselectorNumber;

        public event EventHandler<StateEventArgs> OnState_00;

        public event EventHandler<short> OnFreq_01;

        public event EventHandler<byte> OnGain_02_3b;

        public event EventHandler<GainEventArgs> OnGain_02_4b;

        public event EventHandler<ModePreamp> OnModePreamp_03;

        public event EventHandler<PreselectorOnOff> OnOnOffPreamp_04;

        public event EventHandler<SetPreampRange> OnSetPreampRange_05;

        public event EventHandler<FreqGainEventArgs> OnSetFreqGain_06;

        public event EventHandler<ModePreamp> OnModeExternalSwitches_07;

        public event EventHandler<RFChannel> OnRFChannel_08;

        public event EventHandler<byte> OnGain_09_3b;

        public event EventHandler<GainEventArgs> OnGain_09_4b;

        public event EventHandler<byte> OnAttenuatorLevelLow_0A;

        public event EventHandler<byte> OnAttenuatorLevelHigh_0B;

        public event EventHandler<FeedChannels> OnFeedChannel_0E;

        public event EventHandler<OnOffSwitch> OnOnOffSwitch_0F;

        public event EventHandler<OnOffSwitch> OnOnOffOpticalTransmitter_10;

        public event EventHandler<ModePreamp> OnModeAttenuator_11;

        public event EventHandler<short[]> OnConfigurationChanels_12;

        public event EventHandler<bool> OnSave_13;

        public event EventHandler<bool> OnReset_14;

        public event EventHandler<byte> OnGain_15;

        public event EventHandler<AdditionalStateEventArgs> OnAdditionalState_16;

        public event EventHandler<OnOffSwitch> OnFeedLaser_17;
        #endregion

        /// <summary>
        /// Open COM\TCP type connection
        /// </summary>
        /// <returns>True - connect, False - disconnect</returns>
        public abstract bool Connect();

        /// <summary>
        /// Disconnect COM\TCP
        /// </summary>
        /// <returns>True - connect, False - disconnect</returns>
        public abstract bool Disconnect();
        
        /// <summary>
        /// Query the current state of the preselector
        /// </summary>
        public void GetStatePreselectorCode0()
        {
            const string Message = "00000000000000000000000000000000";
            this.Send(Message);
        }

        /// <summary>
        /// Frequency setting
        /// </summary>
        /// <param name="freqMHz">Freq MHz</param>
        /// <param name="deviceAddress">Device address</param>
        public void SetFreqCode1(short freqMHz, PreselectorNumber deviceAddress)
        {
            var lowByteFreq = string.Empty;
            var highByteFreq = string.Empty;
            if (freqMHz < 30)
            {
                freqMHz = 30;
            }
            else if (freqMHz > 6000)
            {
                freqMHz = 6000;
            }

            var hex = freqMHz.ToString("X");
            if (hex.Length != 0)
            {
                switch (hex.Length)
                {
                    case 2:
                        lowByteFreq = hex;
                        highByteFreq = "00";
                        break;
                    case 3:
                        {
                            var str = hex.Substring(1, 2);
                            lowByteFreq = str;
                            str = hex.Substring(0, 1);
                            highByteFreq = $"0{str}";
                            break;
                        }

                    case 4:
                        {
                            var str = hex.Substring(1, 2);
                            lowByteFreq = str;
                            str = hex.Substring(0, 2);
                            highByteFreq = str;
                            break;
                        }
                }
            }
            var message = $"0{(byte)deviceAddress}01{lowByteFreq}{highByteFreq}";
            this.Send(message);
        }

        /// <summary>
        /// Gain setting
        /// </summary>
        /// <param name="valueGainDb031">Gain 0..31 dB</param>
        /// <param name="deviceAddress">Device address</param>
        public void SetGainCode2_3b(byte valueGainDb031, PreselectorNumber deviceAddress)
        {
            if (valueGainDb031 > 31)
            {
                valueGainDb031 = 31;
            }

            var gain = valueGainDb031.ToString("X");
            if (gain.Length < 2)
            {
                gain = "0" + gain;
            }

            var message = $"0{(byte)deviceAddress}02{gain}";
            this.Send(message);
        }

        /// <summary>
        /// Gain setting
        /// </summary>
        /// <param name="valueGainDb031">First gain 0..31 dB</param>
        /// <param name="valueGain2Db031">Second gain 0..31 dB</param>
        /// <param name="deviceAddress">Device address</param>
        public void SetGainCode2_4b(byte valueGainDb031, byte valueGain2Db031, PreselectorNumber deviceAddress)
        {
            if (valueGainDb031 > 31)
            {
                valueGainDb031 = 31;
            }

            var gain = valueGainDb031.ToString("X");
            if (gain.Length < 2)
            {
                gain = "0" + gain;
            }

            if (valueGain2Db031 > 31)
            {
                valueGain2Db031 = 31;
            }

            var gain2 = valueGain2Db031.ToString("X");
            if (gain2.Length < 2)
            {
                gain2 = "0" + gain2;
            }

            var message = $"0{(byte)deviceAddress}02{gain}{gain2}";
            this.Send(message);
        }

        /// <summary>
        /// Setting the preamplifier operating mode - for all ranges or for the current range
        /// </summary>
        /// <param name="paramPreamp">Mode preamp</param>
        /// <param name="deviceAddress">Device address</param>
        public void SetPreampCode3(ModePreamp paramPreamp, PreselectorNumber deviceAddress)
        {
            var message = $"0{(byte)deviceAddress}030{(byte)paramPreamp}";
            this.Send(message);
        }

        /// <summary>
        /// Preamp on / off
        /// </summary>
        /// <param name="onOffValue">Preselector ON/OFF</param>
        /// <param name="deviceAddress">Device address</param>
        public void SetOnOffCode4(PreselectorOnOff onOffValue, PreselectorNumber deviceAddress)
        {
            var message = $"0{(byte)deviceAddress}040{(byte)onOffValue}";
            this.Send(message);
        }

        /// <summary>
        /// Setting the preselector by range number
        /// </summary>
        /// <param name="preselector">Preselector Range</param>
        /// <param name="deviceAddress">Device address</param>
        public void SetPreselectorSettingCode5(SetPreampRange preselector, PreselectorNumber deviceAddress)
        {
            var hex = ((byte)preselector).ToString("X");
            var message = $"0{(byte)deviceAddress}050{hex}";
            this.Send(message);
        }

        /// <summary>
        /// Setting the frequency and gain at this frequency
        /// </summary>
        /// <param name="freqMHz">Freq MHz</param>
        /// <param name="valueGainDb031">Gain 0..31 dB</param>
        /// <param name="deviceAddress">Device address</param>
        public void SetFreqGainCode6(short freqMHz, byte valueGainDb031, PreselectorNumber deviceAddress)
        {
            var lowByteFreq = string.Empty;
            var highByteFreq = string.Empty;
            if (freqMHz < 30)
            {
                freqMHz = 30;
            }
            else if (freqMHz > 6000)
            {
                freqMHz = 6000;
            }

            var hex = freqMHz.ToString("X");
            if (hex.Length != 0)
            {
                switch (hex.Length)
                {
                    case 2:
                        lowByteFreq = hex;
                        highByteFreq = "00";
                        break;
                    case 3:
                    {
                        var str = hex.Substring(1, 2);
                        lowByteFreq = str;
                        str = hex.Substring(0, 1);
                        highByteFreq = $"0{str}";
                        break;
                    }
                    case 4:
                    {
                        var str = hex.Substring(1, 2);
                        lowByteFreq = str;
                        str = hex.Substring(0, 2);
                        highByteFreq = str;
                        break;
                    }
                }
            }

            if (valueGainDb031 > 31)
            {
                valueGainDb031 = 31;
            }

            var gain = valueGainDb031.ToString("X");
            if (gain.Length < 2)
            {
                gain = "0" + gain;
            }

            var message = $"0{(byte)deviceAddress}06{lowByteFreq}{highByteFreq}{gain}";
            this.Send(message);
        }

        /// <summary>
        /// Setting the operating mode of external switches ZSWA4-63DRB + - for all ranges or for the current range
        /// </summary>
        /// <param name="operationMod">Mode preamp</param>
        /// <param name="deviceAddress">Device address</param>
        public void SetOperationModCode7(ModePreamp operationMod, PreselectorNumber deviceAddress)
        {
            var message = $"0{(byte)deviceAddress}070{(byte)operationMod}";
            this.Send(message);
        }

        /// <summary>
        /// Switching system management with external switches ZSWA4-63DRB +
        /// </summary>
        /// <param name="channel">Channel</param>
        /// <param name="deviceAddress">Device address</param>
        public void SetLinkCode8(RFChannel channel, PreselectorNumber deviceAddress)
        {
            var message = $"0{(byte)deviceAddress}080{(byte)channel}";
            this.Send(message);
        }

        /// <summary>
        /// Setting preamplifier attenuator level, in dB
        /// </summary>
        /// <param name="valueGainDb031">Gain 0..31 dB</param>
        /// <param name="deviceAddress">Device address</param>
        public void SetAttenuatorLevelCode9_3b(byte valueGainDb031, PreselectorNumber deviceAddress)
        {
            if (valueGainDb031 > 31)
            {
                valueGainDb031 = 31;
            }

            var gain = valueGainDb031.ToString("X");
            if (gain.Length < 2)
            {
                gain = "0" + gain;
            }

            var message = $"0{(byte)deviceAddress}09{gain}";
            this.Send(message);
        }

        /// <summary>
        /// Setting preamplifier attenuator level, in dB
        /// </summary>
        /// <param name="valueGainDb031">Gain 0..31 dB</param>
        /// <param name="valueGain2Db031">Gain 0..31 dB</param>
        /// <param name="deviceAddress">Device address</param>
        public void SetAttenuatorLevelCode9_4b(byte valueGainDb031, byte valueGain2Db031, PreselectorNumber deviceAddress)
        {
            if (valueGainDb031 > 31)
            {
                valueGainDb031 = 31;
            }

            var gain = valueGainDb031.ToString("X");
            if (gain.Length < 2)
            {
                gain = "0" + gain;
            }

            if (valueGain2Db031 > 31)
            {
                valueGain2Db031 = 31;
            }

            var gain2 = valueGain2Db031.ToString("X");
            if (gain2.Length < 2)
            {
                gain2 = "0" + gain2;
            }

            var message = $"0{(byte)deviceAddress}09{gain}{gain2}";
            this.Send(message);
        }

        /// <summary>
        /// Setting attenuator level in the lower range, in dB
        /// </summary>
        /// <param name="valueGainDb031">Gain 0..31 dB</param>
        /// <param name="deviceAddress">Device address</param>
        public void SetAttenuatorLowLevelCodeA(byte valueGainDb031, PreselectorNumber deviceAddress)
        {
            if (valueGainDb031 > 31)
            {
                valueGainDb031 = 31;
            }

            var gain = valueGainDb031.ToString("X");
            if (gain.Length < 2)
            {
                gain = "0" + gain;
            }

            var message = $"0{(byte)deviceAddress}0A{gain}";
            this.Send(message);
        }

        /// <summary>
        /// Setting attenuator level in the upper range, in dB
        /// </summary>
        /// <param name="valueGainDb031">Gain 0..31 dB</param>
        /// <param name="deviceAddress">Device address</param>
        public void SetAttenuatorHighLevelCodeB(byte valueGainDb031, PreselectorNumber deviceAddress)
        {
            if (valueGainDb031 > 31)
            {
                valueGainDb031 = 31;
            }

            var gain = valueGainDb031.ToString("X");
            if (gain.Length < 2)
            {
                gain = "0" + gain;
            }

            var message = $"0{(byte)deviceAddress}0B{gain}";
            this.Send(message);
        }

        /// <summary>
        /// Feed Channel
        /// </summary>
        /// <param name="channel">Channel</param>
        /// <param name="deviceAddress">Device address</param>
        public void FeedChannelCodeE(FeedChannels channel, PreselectorNumber deviceAddress)
        {
            var message = $"0{(byte)deviceAddress}0E0{(byte)channel}";
            this.Send(message);
        }

        /// <summary>
        /// Setting power supply for external switches ZSWA4-63DRB+
        /// </summary>
        /// <param name="power">Power</param>
        /// <param name="deviceAddress">Device address</param>
        public void SetPowerCodeF(OnOffSwitch power, PreselectorNumber deviceAddress)
        {
            var message = $"0{(byte)deviceAddress}0F0{(byte)power}";
            this.Send(message);
        }

        /// <summary>
        /// Setting power supply for optical transmitter
        /// </summary>
        /// <param name="power">Power</param>
        /// <param name="deviceAddress">Device address</param>
        public void SetOpticalPowerCode10(OnOffSwitch power, PreselectorNumber deviceAddress)
        {
            var message = $"0{(byte)deviceAddress}100{(byte)power}";
            this.Send(message);
        }

        /// <summary>
        /// Setting the mode of the attenuator in the preamplifier channel - for all ranges or for the current range
        /// </summary>
        /// <param name="range">Range</param>
        /// <param name="deviceAddress">Device address</param>
        public void SetGainOperationModeCode11(ModePreamp range, PreselectorNumber deviceAddress)
        {
            var message = $"0{(byte)deviceAddress}110{(byte)range}";
            this.Send(message);
        }

        /// <summary>
        /// Saving the parameters of the current lane
        /// </summary>
        public void SetSaveCode13()
        {
            const string Message = "0013";
            this.Send(Message);
        }

        /// <summary>
        /// Configuring channels for the switching system on external switches ZSWA4-63DRB +
        /// </summary>
        /// <param name="byteChannel">String Channel</param>
        /// <param name="deviceAddress">Device address</param>
        public void SetExternalSwitchCode12(string byteChannel, PreselectorNumber deviceAddress)
        {
            if (byteChannel.Length % 2 != 0)
            {
                byteChannel = "41322314";
            }

            var message = $"0{(byte)deviceAddress}12{byteChannel}";
            this.Send(message);
        }

        /// <summary>
        /// Reset preselector
        /// </summary>
        public void ResetCode14()
        {
            const string Message = "0014";
            this.Send(Message);
        }

        /// <summary>
        /// Setting gain
        /// </summary>
        /// <param name="valueGainDb0255">Gain 0..255</param>
        /// <param name="deviceAddress">Device address</param>
        public void SetPathGainCode15(byte valueGainDb0255, PreselectorNumber deviceAddress)
        {
            if (valueGainDb0255 > 255)
            {
                valueGainDb0255 = 255;
            }

            var gain = valueGainDb0255.ToString("X");
            if (gain.Length < 2)
            {
                gain = "0" + gain;
            }

            var message = $"0{(byte)deviceAddress}15{gain}";
            this.Send(message);
        }

        /// <summary>
        /// Additional settings
        /// </summary>
        public void AdditionalSettingsCode16()
        {
            const string Message = "0016000000000000000000";
            this.Send(Message);
        }

        /// <summary>
        /// Setting laser power
        /// </summary>
        /// <param name="power">Power On\Off</param>
        /// <param name="deviceAddress">Device address</param>
        public void SetLaserPowerCode17(OnOffSwitch power, PreselectorNumber deviceAddress)
        {
            var message = $"0{(byte)deviceAddress}170{(byte)power}";
            this.Send(message);
        }

        /// <summary>
        /// Send some command
        /// </summary>
        /// <param name="message"></param>
        public void SetCommand(string message)
        {
            if (message != string.Empty)
            {
                this.Send(message);
            }
        }

        /// <summary>
        /// Convert byte array to correct string.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="buffer">
        /// Byte array.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        protected static string ConvertToString(string message, byte[] buffer)
        {
            if (message == null)
            {
                return null;
            }

            message = BitConverter.ToString(buffer).Replace("-", string.Empty);
            message = string.Join(string.Empty, message.ToCharArray().Select((c, i) => i % 2 == 0 ? c.ToString() : $"{c} "));
            return message;
        }

        /// <summary>
        /// Get second byte.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        protected static string SecondByte(string message)
        {
            var charMes = message.ToCharArray();
            var secondByte = $"{charMes[2]}{charMes[3]}";
            return secondByte;
        }

        /// <summary>
        /// Used by commands, implemented separately for each type of connection
        /// </summary>
        /// <param name="message">Command for Send</param>
        protected abstract void Send(string message);

        /// <summary>
        /// Decryption answer.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        protected void DecryptionAnswer(string message)
        {
            var str = message.Substring(0, 2);
            var value = Convert.ToInt16(str);
            var preselectorNumber = (PreselectorNumber)value;
            this.OnPreselectorNumber?.Invoke(this, preselectorNumber);
            str = message.Substring(3, 2);
            if (message.Length >= 34)
            {
                this.AllDecryption(message, str);
            }
            else if (str != "00")
            {
                this.AllDecryption(message, str);
            }
        }

        /// <summary>
        /// The set additional.
        /// </summary>
        /// <param name="state">
        /// The state.
        /// </param>
        /// <param name="additional">
        /// The additional.
        /// </param>
        private static void SetAdditional(string state, AdditionalStateEventArgs additional)
        {
            var binary = string.Join(
                string.Empty,
                state.Select(c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')));
            var bite = binary.Substring(7, 1);
            int valueInt = Convert.ToInt16(bite, 2);
            additional.Preamp = (OnOffSwitch)valueInt;
            bite = binary.Substring(6, 1);
            valueInt = Convert.ToInt16(bite, 2);
            additional.GainRange = (ModPreamp)valueInt;
            bite = binary.Substring(5, 1);
            valueInt = Convert.ToInt16(bite, 2);
            additional.StateSwitches = (ModPreamp)valueInt;
            bite = binary.Substring(3, 2);
            valueInt = Convert.ToInt16(bite, 2);
            additional.StatePreamp = (PreselectorOnOff)valueInt;
            bite = binary.Substring(2, 1);
            valueInt = Convert.ToInt16(bite, 2);
            additional.PowerFirstPreselector = (OnOffSwitch)valueInt;
            bite = binary.Substring(1, 1);
            valueInt = Convert.ToInt16(bite, 2);
            additional.PowerSecondPreselector = (OnOffSwitch)valueInt;
            bite = binary.Substring(0, 1);
            valueInt = Convert.ToInt16(bite, 2);
            additional.StatePowerLaser = (OnOffSwitch)valueInt;
        }

        /// <summary>
        /// The set preamp.
        /// </summary>
        /// <param name="state">
        /// The state.
        /// </param>
        /// <param name="State">
        /// The state.
        /// </param>
        private static void SetPreamp(string state, StateEventArgs State)
        {
            var binary = string.Join(
                string.Empty,
                state.Select(c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')));
            var bite = binary.Substring(0, 1);
            int valueInt = Convert.ToInt16(bite, 2);
            State.Power1 = (FlagOnOff)valueInt;

            bite = binary.Substring(1, 7);
            valueInt = Convert.ToInt16(bite, 2);
            State.StateFilter1 = (FreqLO)valueInt;
        }

        /// <summary>
        /// The set preamp 2.
        /// </summary>
        /// <param name="state">
        /// The state.
        /// </param>
        /// <param name="State">
        /// The state.
        /// </param>
        private static void SetPreamp2(string state, StateEventArgs State)
        {
            var binary = string.Join(
                string.Empty,
                state.Select(c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')));
            var bite = binary.Substring(0, 1);
            int valueInt = Convert.ToInt16(bite, 2);
            State.Power2 = (FlagOnOff)valueInt;

            bite = binary.Substring(1, 7);
            valueInt = Convert.ToInt16(bite, 2);
            State.StateFilter2 = (FreqLO)valueInt;
        }

        /// <summary>
        /// The switch state.
        /// </summary>
        /// <param name="state">
        /// The state.
        /// </param>
        /// <param name="State">
        /// The state.
        /// </param>
        private static void SwitchState(string state, StateEventArgs State)
        {
            var binary = string.Join(
                string.Empty,
                state.Select(c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')));

            var bite = binary.Substring(5, 3);
            int valueInt = Convert.ToInt16(bite, 2);
            State.ExternalSwitch1 = (ExternalSwitchState)valueInt;

            bite = binary.Substring(2, 3);
            valueInt = Convert.ToInt16(bite, 2);
            State.ExternalSwitch2 = (ExternalSwitchState)valueInt;

            bite = binary.Substring(1, 1);
            valueInt = Convert.ToInt16(bite, 2);
            State.Power3_3V = (FlagOnOff)valueInt;

            bite = binary.Substring(0, 1);
            valueInt = Convert.ToInt16(bite, 2);
            State.Power12V = (FlagOnOff)valueInt;
        }

        /// <summary>
        /// All decryption.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="str">
        /// The str.
        /// </param>
        private void AllDecryption(string message, string str)
        {
            if (str == null)
            {
                return;
            }

            string value;
            short valueInt;
            str = message.Substring(3, 2);
            switch (str)
            {
                case "00" when message.Length >= 40:
                    {
                        var state = new StateEventArgs();

                        var stateMessage = message.Substring(6, 2);
                        var decValue = Convert.ToInt32(stateMessage, 16);
                        state.GainLO1 = (byte)decValue;

                        stateMessage = message.Substring(9, 2);
                        decValue = Convert.ToInt32(stateMessage, 16);
                        state.GainHI1 = (byte)decValue;

                        stateMessage = message.Substring(12, 2);
                        decValue = Convert.ToInt32(stateMessage, 16);
                        state.GainPreamp1 = (byte)decValue;

                        stateMessage = message.Substring(15, 2);
                        decValue = Convert.ToInt32(stateMessage, 16);
                        state.StatePreamp1 = (NumberPreampFilter)decValue;

                        stateMessage = message.Substring(18, 2);
                        SetPreamp(stateMessage, state);

                        stateMessage = message.Substring(21, 2);
                        SwitchState(stateMessage, state);

                        stateMessage = message.Substring(24, 2);
                        decValue = Convert.ToInt32(stateMessage, 16);
                        state.GainLO2 = (byte)decValue;

                        stateMessage = message.Substring(27, 2);
                        decValue = Convert.ToInt32(stateMessage, 16);
                        state.GainHI2 = (byte)decValue;

                        stateMessage = message.Substring(30, 2);
                        decValue = Convert.ToInt32(stateMessage, 16);
                        state.GainPreamp2 = (byte)decValue;

                        stateMessage = message.Substring(33, 2);
                        decValue = Convert.ToInt32(stateMessage, 16);
                        state.StatePreamp2 = (NumberPreampFilter)decValue;

                        stateMessage = message.Substring(36, 2);
                        SetPreamp2(stateMessage, state);

                        stateMessage = message.Substring(39, 2);
                        var lowByte = (byte)Convert.ToInt32(stateMessage, 16);
                        stateMessage = message.Substring(42, 2);
                        var highByte = (byte)Convert.ToInt32(stateMessage, 16);
                        state.LaserPower = BitConverter.ToInt16(new[] { lowByte, highByte }, 0);

                        stateMessage = message.Substring(45, 2);
                        state.Temperature = Convert.ToInt16(stateMessage, 16);
                        this.OnState_00?.Invoke(this, state);
                        break;
                    }
                case "16":
                    {
                        var additional = new AdditionalStateEventArgs();
                        var state = message.Substring(6, 2);
                        var decValue = Convert.ToInt32(state, 16);
                        additional.Gain = (byte)decValue;

                        state = message.Substring(9, 2);
                        SetAdditional(state, additional);

                        state = message.Substring(12, 2);
                        decValue = Convert.ToInt32(state, 16);
                        additional.AmplifaerLevel = (byte)decValue;
                        this.OnAdditionalState_16?.Invoke(this, additional);
                        break;
                    }
                case "01":
                    {
                        if (message.Length > 10)
                        {
                            var state = message.Substring(6, 2);
                            var lowByte = (byte)Convert.ToInt32(state, 16);
                            state = message.Substring(9, 2);
                            var highByte = (byte)Convert.ToInt32(state, 16);
                            this.OnFreq_01?.Invoke(this, BitConverter.ToInt16(new[] { lowByte, highByte }, 0));
                        }

                        break;
                    }
                case "02" when message.Length > 7 && message.Length < 10:
                    value = message.Substring(6, 2);
                    valueInt = Convert.ToInt16(value, 16);
                    this.OnGain_02_3b?.Invoke(this, (byte)valueInt);
                    break;
                case "02":
                    {
                        if (message.Length > 10)
                        {
                            value = message.Substring(6, 2);
                            valueInt = Convert.ToInt16(value, 16);
                            value = message.Substring(9, 2);
                            int valueInt2 = Convert.ToInt16(value, 16);
                            this.OnGain_02_4b?.Invoke(this, new GainEventArgs((byte)valueInt, (byte)valueInt2));
                        }

                        break;
                    }
                case "03":
                    {
                        if (message.Length > 7)
                        {
                            value = message.Substring(6, 2);
                            valueInt = Convert.ToInt16(value, 16);
                            this.OnModePreamp_03?.Invoke(this, (ModePreamp)valueInt);
                        }

                        break;
                    }
                case "04":
                    {
                        if (message.Length > 7)
                        {
                            value = message.Substring(6, 2);
                            valueInt = Convert.ToInt16(value, 16);
                            this.OnOnOffPreamp_04?.Invoke(this, (PreselectorOnOff)valueInt);
                        }

                        break;
                    }
                case "05":
                    {
                        if (message.Length > 7)
                        {
                            value = message.Substring(6, 2);
                            valueInt = Convert.ToInt16(value, 16);
                            this.OnSetPreampRange_05?.Invoke(this, (SetPreampRange)valueInt);
                        }

                        break;
                    }
                case "06":
                    {
                        if (message.Length > 13)
                        {
                            var state = message.Substring(6, 2);
                            var lowByte = (byte)Convert.ToInt32(state, 16);
                            state = message.Substring(9, 2);
                            var highByte = (byte)Convert.ToInt32(state, 16);
                            value = message.Substring(12, 2);
                            valueInt = Convert.ToInt16(value, 16);
                            this.OnSetFreqGain_06?.Invoke(this, new FreqGainEventArgs(BitConverter.ToInt16(new[] { lowByte, highByte }, 0), (byte)valueInt));
                        }

                        break;
                    }
                case "07":
                    {
                        if (message.Length > 7)
                        {
                            value = message.Substring(6, 2);
                            valueInt = Convert.ToInt16(value, 16);
                            this.OnModeExternalSwitches_07?.Invoke(this, (ModePreamp)valueInt);
                        }

                        break;
                    }
                case "08":
                    {
                        if (message.Length > 7)
                        {
                            value = message.Substring(6, 2);
                            valueInt = Convert.ToInt16(value, 16);
                            this.OnRFChannel_08?.Invoke(this, (RFChannel)valueInt);
                        }

                        break;
                    }
                case "09" when message.Length > 7 && message.Length < 10:
                    value = message.Substring(6, 2);
                    valueInt = Convert.ToInt16(value, 16);
                    this.OnGain_09_3b?.Invoke(this, (byte)valueInt);
                    break;
                case "09":
                    {
                        if (message.Length > 10)
                        {
                            value = message.Substring(6, 2);
                            valueInt = Convert.ToInt16(value, 16);
                            value = message.Substring(9, 2);
                            int valueInt2 = Convert.ToInt16(value, 16);
                            this.OnGain_09_4b?.Invoke(this, new GainEventArgs((byte)valueInt, (byte)valueInt2));
                        }

                        break;
                    }
                case "0A":
                    {
                        if (message.Length > 7)
                        {
                            value = message.Substring(6, 2);
                            valueInt = Convert.ToInt16(value, 16);
                            this.OnAttenuatorLevelLow_0A?.Invoke(this, (byte)valueInt);
                        }

                        break;
                    }
                case "0B":
                    {
                        if (message.Length > 7)
                        {
                            value = message.Substring(6, 2);
                            valueInt = Convert.ToInt16(value, 16);
                            this.OnAttenuatorLevelHigh_0B?.Invoke(this, (byte)valueInt);
                        }

                        break;
                    }
                case "0E":
                    {
                        if (message.Length > 7)
                        {
                            value = message.Substring(6, 2);
                            valueInt = Convert.ToInt16(value, 16);
                            this.OnFeedChannel_0E?.Invoke(this, (FeedChannels)valueInt);
                        }

                        break;
                    }
                case "0F":
                    {
                        if (message.Length > 7)
                        {
                            value = message.Substring(6, 2);
                            valueInt = Convert.ToInt16(value, 16);
                            this.OnOnOffSwitch_0F?.Invoke(this, (OnOffSwitch)valueInt);
                        }

                        break;
                    }
                case "10":
                    {
                        if (message.Length > 7)
                        {
                            value = message.Substring(6, 2);
                            valueInt = Convert.ToInt16(value, 16);
                            this.OnOnOffOpticalTransmitter_10?.Invoke(this, (OnOffSwitch)valueInt);
                        }

                        break;
                    }
                case "12":
                    {
                        if (message.Length > 15)
                        {
                            value = message.Substring(6, 2);
                            var valueInt1 = Convert.ToInt16(value);
                            value = message.Substring(9, 2);
                            var valueInt2 = Convert.ToInt16(value);
                            value = message.Substring(12, 2);
                            var valueInt3 = Convert.ToInt16(value);
                            value = message.Substring(15, 2);
                            var valueInt4 = Convert.ToInt16(value);
                            var k = new[] { valueInt1, valueInt2, valueInt3, valueInt4 };
                            this.OnConfigurationChanels_12?.Invoke(this, k);
                        }

                        break;
                    }
                case "11":
                    {
                        if (message.Length > 7)
                        {
                            value = message.Substring(6, 2);
                            valueInt = Convert.ToInt16(value, 16);
                            this.OnModeAttenuator_11?.Invoke(this, (ModePreamp)valueInt);
                        }

                        break;
                    }
                case "13":
                    this.OnSave_13?.Invoke(this, true);
                    break;
                case "14":
                    this.OnReset_14?.Invoke(this, true);
                    break;
                case "15":
                    {
                        if (message.Length > 7)
                        {
                            value = message.Substring(6, 2);
                            valueInt = Convert.ToInt16(value, 16);
                            this.OnGain_15?.Invoke(this, (byte)valueInt);
                        }

                        break;
                    }
                case "17":
                    {
                        if (message.Length > 7)
                        {
                            value = message.Substring(6, 2);
                            valueInt = Convert.ToInt16(value, 16);
                            this.OnFeedLaser_17?.Invoke(this, (OnOffSwitch)valueInt);
                        }

                        break;
                    }
            }
        }
        
    }
}
