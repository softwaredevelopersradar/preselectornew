﻿using System;
using System.Linq;
using SimpleTCP;

namespace WorkPortNew
{
    /// <summary>
    /// TCP type connection.
    /// </summary>
    public class PreselTcp : PreselModel
    {
        /// <summary>
        /// The client.
        /// </summary>
        private SimpleTcpClient client;

        /// <summary>
        /// Bytes sent via TCP.
        /// </summary>
        public override event EventHandler<string> OnWrite;

        /// <summary>
        /// Bytes read via tcp.
        /// </summary>
        public override event EventHandler<string> OnRead;

        /// <summary>
        /// Initializes a new instance of the <see cref="PreselTcp"/> class.
        /// </summary>
        /// <param name="ip">
        /// IP for Tcp.
        /// </param>
        /// <param name="port">
        /// Port for Tcp.
        /// </param>
        public PreselTcp(string ip, int port)
        {
            this.client = new SimpleTcpClient();
            this.Ip = ip;
            this.Port = port;
        }

        /// <summary>
        /// Gets or sets the IP.
        /// </summary>
        private string Ip { get; set; }

        /// <summary>
        /// Gets or sets the port.
        /// </summary>
        private int Port { get; set; }
        
        /// <summary>
        /// The connect.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool Connect()
        {
            try
            {
                this.client = new SimpleTcpClient().Connect(this.Ip, this.Port);
                this.client.DataReceived += (sender, args) => { OnRead.Invoke(sender, args.MessageString); };
                return true;
            }
            catch
            {
                this.client.Dispose();
                return false;
            }
        }

        /// <summary>
        /// Disconnect Tcp.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool Disconnect()
        {
            try
            {
                this.client.DataReceived -= (sender, args) => { OnRead.Invoke(sender, args.MessageString); };
                this.client.Disconnect();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Send message.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        protected override void Send(string message)
        {
            var messageMass = message.Split(' ');
            message = string.Join(string.Empty, messageMass);
            var buffer = new byte[messageMass.Length];
            buffer = Enumerable.Range(0, message.Length)
                .Where(x => x % 2 == 0)
                .Select(x => Convert.ToByte(message.Substring(x, 2), 16))
                .ToArray();
            try
            {
                if (this.client != null)
                {
                    this.client.Write(buffer);
                    this.OnWrite?.Invoke(this, message);
                }
            }
            catch
            {
                // ignored
            }
        }
        
    }
}
