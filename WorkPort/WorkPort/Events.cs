﻿using System;

namespace WorkPortNew
{
    public class StateEventArgs : EventArgs
    {
        public byte GainLO1 { get; set; }
        public byte GainHI1 { get; set; }
        public byte GainPreamp1 { get; set; }
        public NumberPreampFilter StatePreamp1 { get; set; }
        public FreqLO StateFilter1 { get; set; }
        public FlagOnOff Power1 { get; set; }
        public ExternalSwitchState ExternalSwitch1 { get; set; }
        public ExternalSwitchState ExternalSwitch2 { get; set; }
        public FlagOnOff Power3_3V { get; set; }
        public FlagOnOff Power12V { get; set; }
        public byte GainLO2 { get; set; }
        public byte GainHI2 { get; set; }
        public byte GainPreamp2 { get; set; }
        public NumberPreampFilter StatePreamp2 { get; set; }
        public FreqLO StateFilter2 { get; set; }
        public FlagOnOff Power2 { get; set; }
        public short LaserPower { get; set; }
        public short Temperature { get; set; }

        public StateEventArgs(byte GainLO1, byte GainHI1, byte GainPreamp1, NumberPreampFilter StatePreamp1, FreqLO StateFilter1, FlagOnOff Power1, ExternalSwitchState ExternalSwitch1, ExternalSwitchState ExternalSwitch2,
            FlagOnOff Power3_3V, FlagOnOff Power12V, byte GainLO2, byte GainHI2, byte GainPreamp2, NumberPreampFilter StatePreamp2, FreqLO StateFilter2, FlagOnOff Power2, short LaserPower, short Temperature)
        {
            this.GainLO1 = GainLO1;
            this.GainHI1 = GainHI1;
            this.GainPreamp1 = GainPreamp1;
            this.StatePreamp1 = StatePreamp1;
            this.StateFilter1 = StateFilter1;
            this.Power1 = Power1;
            this.ExternalSwitch1 = ExternalSwitch1;
            this.ExternalSwitch2 = ExternalSwitch2;
            this.Power3_3V = Power3_3V;
            this.Power12V = Power12V;
            this.GainLO2 = GainLO2;
            this.GainHI2 = GainHI2;
            this.GainPreamp2 = GainPreamp2;
            this.StatePreamp2 = StatePreamp2;
            this.StateFilter2 = StateFilter2;
            this.Power2 = Power2;
            this.LaserPower = LaserPower;
            this.Temperature = Temperature;
        }

        public StateEventArgs()
        {
            GainLO1 = 0;
            GainHI1 = 0;
            GainPreamp1 = 0;
            StatePreamp1 = 0;
            StateFilter1 = 0;
            Power1 = 0;
            ExternalSwitch1 = 0;
            ExternalSwitch2 = 0;
            Power3_3V = 0;
            Power12V = 0;
            GainLO2 = 0;
            GainHI2 = 0;
            GainPreamp2 = 0;
            StatePreamp2 = 0;
            StateFilter2 = 0;
            Power2 = 0;
            LaserPower = 0;
            Temperature = 0;
        }
    }

    public class AdditionalStateEventArgs : EventArgs
    {
        public byte Gain { get; set; }
        public OnOffSwitch Preamp { get; set; }
        public ModPreamp GainRange { get; set; }
        public ModPreamp StateSwitches { get; set; }
        public PreselectorOnOff StatePreamp { get; set; }
        public OnOffSwitch PowerFirstPreselector { get; set; }
        public OnOffSwitch PowerSecondPreselector { get; set; }
        public OnOffSwitch StatePowerLaser { get; set; }
        public byte AmplifaerLevel { get; set; }

        public AdditionalStateEventArgs()
        {
            Gain = 0;
            Preamp = 0;
            GainRange = 0;
            StateSwitches = 0;
            StatePreamp = 0;
            StatePowerLaser = 0;
            AmplifaerLevel = 0;
        }

        public AdditionalStateEventArgs(byte Gain, OnOffSwitch Preamp, ModPreamp GainRange, ModPreamp StateSwitches, PreselectorOnOff StatePreamp, OnOffSwitch PowerFirstPreselector, OnOffSwitch PowerSecondPreselector, OnOffSwitch StatePowerLaser, byte AmplifaerLevel)
        {
            this.Gain = Gain;
            this.Preamp = Preamp;
            this.GainRange = GainRange;
            this.StateSwitches = StateSwitches;
            this.PowerFirstPreselector = PowerFirstPreselector;
            this.PowerSecondPreselector = PowerSecondPreselector;
            this.StatePreamp = StatePreamp;
            this.StatePowerLaser = StatePowerLaser;
            this.AmplifaerLevel = AmplifaerLevel;
        }
    }

    public class FreqGainEventArgs : EventArgs
    {
        public short Freq { get; set; }
        public byte Gain { get; set; }

        public FreqGainEventArgs(short Freq, byte Gain)
        {
            this.Freq = Freq;
            this.Gain = Gain;
        }
    }

    public class  GainEventArgs : EventArgs
    {
        public byte Gain_1 { get; set; }
        public byte Gain_2 { get; set; }

        public GainEventArgs(byte Gain_1, byte Gain_2)
        {
            this.Gain_1 = Gain_1;
            this.Gain_2 = Gain_2;
        }
    }
}
